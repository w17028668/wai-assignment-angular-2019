<?php

class DB{

    public function connect()
    {
        $fileName = "nfc_films_v2.sqlite";

        try {

            $conn = new PDO("sqlite:".__DIR__."/".$fileName);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $conn;

        } catch (PDOException $e) {

            die("Connection failed: " . $e->getMessage());

        }

    }
}
