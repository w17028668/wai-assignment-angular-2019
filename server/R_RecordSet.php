<?php

require_once("DB.php");

abstract class R_RecordSet {
    protected $db;
    protected $stmt;

    
 
    function __construct() {
        
        $this->db = DB::connect();
    }
 
    /**
     * @param string $sql    The sql for the recordset
     * @param array $params  An optional associative array if you want a prepared statement
     * @return PDO_STATEMENT
     */
    function getRecordSet($sql, $elementName = 'ResultSet', $params = null) {
        if (is_array($params)) {
            $this->stmt = $this->db->prepare($sql);
            // execute the statement passing in the named placeholder and the value it'll have
            $this->stmt->execute($params);
        }
        else {
            $this->stmt = $this->db->query($sql);
        }
        return $this->stmt;
    }
}