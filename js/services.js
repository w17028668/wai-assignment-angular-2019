(function () {
    'use strict';
    /** Service to return the data */
     
    angular.module('waiAssignment').
        service('dataService',         // the data service name, can be anything we want
            ['$q',                     // dependency, $q handles promises, the request initially returns a promise, not the data
             '$http',                  // dependency, $http handles the ajax request
             function($q, $http) {     // the parameters must be in the same order as the dependencies
     
                /*
                 * var to hold the data base url
                 */
                var urlBase = '/wai-assignment/server/index.php';
     
                /*
                 * method to retrieve courses, or, more accurately a promise which when
                 * fulfilled calls the success method
                 */
                this.getCourses = function () {
                    var defer = $q.defer(),             // The promise
                        data = {                        // the data to be passed to the url
                            action: 'list',
                            subject: 'courses'
                        };
                    /**
                     * make an ajax get call 
                     * chain calls to .success and .error which will resolve or reject the promise
                     * @param {string} urlBase The url to call, later we'll to this to pass parameters
                     * @param {object} config a configuration object, can contain parameters to pass, in this case we set cache to true
                     * @return {object} promise The call returns, not data, but a promise which only if the call is successful is 'honoured'
                     */
                    $http.get(urlBase, {params: data, cache: true}).                          // notice the dot to start the chain to success()
                            success(function(response){
                               
                                defer.resolve({
                                                data: response.ResultSet.Result,         // create data property with value from response
                                                rowCount: response.courseCount  // create rowCount property with value from response
                                              });
                            }).                                                 // another dot to chain to error()
                            error(function(err){
                                defer.reject(err);
                            });
                    // the call to getCourses returns this promise which is fulfilled 
                    // by the .get method .success or .failure
                    return defer.promise;
                };

                this.getStudents = function (courseCode) {
                    var defer = $q.defer(),
                        data = {                        // the data to be passed to the url
                            action: 'list',
                            subject: 'students',
                            id: courseCode
                        };
                 
                    $http.get(urlBase, {params: data, cache: true}).                     // notice the dot to start the chain to success()
                            success(function(response){
                                console.log(response);
                                defer.resolve({
                                                data: response.ResultSet.Result,         // create data property with value from response
                                                rowCount: response.studentCount // create rowCount property with value from response
                                              });
                            }).                                         // another dot to chain to error()
                            error(function(err){
                                defer.reject(err);
                            });
                    // the call to getCourses returns this promise which is fulfilled 
                    // by the .get method .success or .failure
                    return defer.promise;
                };

                /**
                 * By default post sends data as application/json you need to make sure your server handles that
                 * @param student
                 * @returns {promise|*}
                 */
                this.updateStudent = function (student) {
                    var defer = $q.defer(),
                        data = {
                            action: 'update',
                            subject: 'student',
                            data: angular.toJson(student)
                        };
                
                    $http.post(urlBase, data).
                        success(function(response){
                            defer.resolve(response);
                        }).
                        error(function (err){
                            defer.reject(err);
                        });
                    return defer.promise;
                };
     
             }
            ]
        );
    }());