(function () {
    "use strict";
     
    /**
     * Extend the module 'CourseApp' instantiated in app.js  To add a controller called
     * IndexController (on line 17)
     * 
     * The controller is given two parameters, a name as a string and an array.
     * The array lists any injected objects to add and then a function which will be the
     * controller object and can contain properties and methods.
     * $scope is a built in object which refers to the application model and acts 
     * as a sort of link between the controller, its data and the application's views.
     * '
     * @link https://docs.angularjs.org/guide/scope
     */
    angular.module('waiAssignment').
        controller('IndexController',   // controller given two params, a name and an array
            [
                '$scope',               // angular variable as a string 
                function ($scope) {
                    // add a title property which we can refer to in our view (index.html in this example)
                    $scope.title = 'Course & Student Information';
                }
            ]
             
            ).
            controller('CourseController',  // create a CourseController
                [
                '$scope',
                'dataService',
                '$location',
                function ($scope, dataService, $location) {
                    var getCourses = function () {
                            dataService.getCourses().then(  // then() is called when the promise is resolve or rejected
                                function(response){
                                    $scope.courseCount  = response.rowCount + ' courses';
                                    $scope.courses      = response.data;
                                    //console.log(response.data);
                                },
                                function(err){
                                    $scope.status = 'Unable to load data ' + err;
                                },
                                function(notify){
                                    console.log(notify);
                                }
                            ); // end of getCourses().then
                        };
                        $scope.selectedCourse = {};

                        $scope.selectCourse = function ($event, course) {
                            console.log(course);
                            $scope.selectedCourse = course;
                            $location.path('/courses/' + course);
                        }     
                    getCourses();  // call the method just defined
                }
            ]
        ).
        controller('CourseStudentsController', 
        [
            '$scope', 
            'dataService', 
            '$routeParams',
             
            function ($scope, dataService, $routeParams){
                $scope.students = [ ];
                $scope.studentCount = 0;
 
                var getStudents = function (coursecode) {
                    dataService.getStudents(coursecode).then(
                        function (response) {
                            $scope.studentCount = response.rowCount + ' students';
                            $scope.students = response.data;
                        },
                        function (err){
                            $scope.status = 'Unable to load data ' + err;
                        }
                    );  // end of getStudents().then
                };
 
                // only if there has been a courseid passed in do we bother trying to get the students
                if ($routeParams && $routeParams.courseid) {
                    console.log($routeParams.courseid);
                    getStudents($routeParams.courseid);
                }
                  
                /**
                 * Shows the edit window and positions it based on the row clicked on.
                 * 
                 * @param {object} $event
                 * @param {object} student
                 * @returns {null}
                 */
                $scope.showEditStudent = function ($event, student, editorID) {
                    var element = $event.currentTarget,
                        padding = 22,
                        posY = (element.offsetTop + element.clientTop + padding) - (element.scrollTop + element.clientTop),
                        studentEditorElement = document.getElementById(editorID);
                
                    console.log(student);
                    $scope.selectedStudent = angular.copy(student);
                    $scope.editorVisible = true;
                
                    studentEditorElement.style.position = 'absolute';
                    studentEditorElement.style.top = posY + 'px';
                };

                /**
                 * Abandon the edit in progress
                 * @returns {null}
                 */
                $scope.abandonEdit = function () {
                    $scope.editorVisible = false;
                    $scope.selectedStudent = null;
                };
                
                /**
                 * functions, attached to $scope so they're visible in the view, 
                 * to handle editing a student
                 * 
                 * @param {object} student
                 * @returns {null}
                 */
                $scope.saveStudent = function (){
                    var n,
                        scount = $scope.students.length,
                        currentStudent;
                
                    $scope.editorVisible = false;
                    // call dataService method
                    dataService.updateStudent($scope.selectedStudent).then(
                        function (response) {
                            $scope.status = response.status;
                            if (response.status === 'ok') { // if we saved the file then update the screen
                                for (n = 0; n < scount; n += 1) {
                                    currentStudent = $scope.students[n];
                                    if (currentStudent.studentid === $scope.selectedStudent.studentid) {
                                        $scope.students[n] = angular.copy($scope.selectedStudent);
                                        break;
                                    }
                                }
                            }
                            console.log(response);
                            // reset selectedStudent
                            $scope.selectedStudent = null;
                        },
                        function (err) {
                            $scope.status = "Error with save " + err;
                        }
                    );
                };
            }
        ]
    );
}());